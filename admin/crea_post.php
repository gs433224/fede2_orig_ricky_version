<?php include "../includes/config_locale.php"; ?>			

<?php include "../header.html"; ?>
        <title>Servizi - Fedé - Estetica & Dedizione | Vittorio Veneto</title><!--titolo-->
    </head>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge" />
			<meta name="theme-color" content="#080D10"><!--color mobile head-->
			<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />
			<!-- favicon icon -->
			<link rel="shortcut icon" href="../images/favicon.png">
			<link rel="apple-touch-icon" sizes="57x57" href="../images/apple-touch-icon-57x57.png">
			<link rel="apple-touch-icon" sizes="72x72" href="../images/apple-touch-icon-72x72.png">
			<link rel="apple-touch-icon" sizes="114x114" href="../images/apple-touch-icon-114x114.png">
			<!-- style sheets  -->
			<link rel="stylesheet" type="text/css" href="../style/style.css" />
			<link rel="stylesheet" type="text/css" href="../style/bootstrap.css" />
	
			<meta name="robots" content="index,nofollow">
			<meta name="author" content="Spring ADV">
			<meta name="copyright" content="Carlo Minot"><!--cliente-->
			<script src="https://cdn.tiny.cloud/1/w5qz1bjmccfhoyed8b4qs3azklwl1pkd2oqpf2is9hdcuzm6/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
			<script>tinymce.init({ selector: 'textarea', menubar: '', content_style: "p { margin: 0; }", force_br_newlines : true, force_p_newlines : false, forced_root_block : false, branding: false});</script>

		</head>
        <body class="page admin">
			<div class="mx-3 px-sm-0 pt-3">
				<p class="float-right"><a href="/" title="visita sito" target="_blank">Visita il sito</a> | <a href="login.php" title="Logout">Logout</a></p>
                <img class="logo_login" src="../images/logo-dark.png" alt="logo">
			</div>
			<div class="container">
				<div class="row">
					<div class="col-12">
                        <h2>Crea nuovo post</h2>
                        <form action="crea_post.php" id="form_crea_post" enctype="multipart/form-data" method="post" class="w-100">
                            <div class="form-group">
                                <label>Titolo del post</label><br>
                                <input class="w-100" type="text" name="title_post" required>
                            </div>
                            <div class="form-group">
                                <textarea name="article_post" placeholder="Testo del post"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Immagine del post</label><br>
                                <input type="file" name="files[]" multiple> 
                            </div>
                            <div class="form-group">
                                <label>Data di pubblicazione</label><br>
                                <input type="date" name="post_date" required><br>
                            </div>
                            <div class="clearfix"></div>
                            <input type="submit" name="create_post" value="Pubblica post">
                            <a class="annulla" href="../admin/index.php">Annulla</a>
                        </form>
                    </div>
                </div>
            </div>
        </body>
    </html>

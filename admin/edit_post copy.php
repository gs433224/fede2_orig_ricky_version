<?php 
	include "../includes/config_locale.php"; 
	//recupera id
	if(isset($_REQUEST['id'])) {
		$id = $_REQUEST['id'];
		$sql = "SELECT * FROM posts WHERE id= $id";
		$result = $conn->query($sql); 

		$conn->close();
	}						
?>			

<!doctype html>
	<html lang="it">
		<head>
			<title>Modifica post | Carlo Minot - Campione europeo di Enduro</title>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge" />
			<meta name="theme-color" content="#080D10"><!--color mobile head-->
			<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />
			<!-- favicon icon -->
			<link rel="shortcut icon" href="../images/favicon.png">
			<link rel="apple-touch-icon" sizes="57x57" href="../images/apple-touch-icon-57x57.png">
			<link rel="apple-touch-icon" sizes="72x72" href="../images/apple-touch-icon-72x72.png">
			<link rel="apple-touch-icon" sizes="114x114" href="../images/apple-touch-icon-114x114.png">
			<!-- style sheets  -->
			<link rel="stylesheet" type="text/css" href="../style/style.css" />
			<link rel="stylesheet" type="text/css" href="../style/bootstrap.css" />
	
			<meta name="robots" content="index,nofollow">
			<meta name="author" content="Spring ADV">
			<meta name="copyright" content="Carlo Minot"><!--cliente-->
			<script src="https://cdn.tiny.cloud/1/w5qz1bjmccfhoyed8b4qs3azklwl1pkd2oqpf2is9hdcuzm6/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
			<script>tinymce.init({ selector: 'textarea', menubar: '', content_style: "p { margin: 0; }", branding: false});</script>

		</head>
        <body class="page admin">
			<div class="mx-3 px-sm-0 pt-3">
				<p class="float-right"><a href="/" title="visita sito" target="_blank">Visita il sito</a> | <a href="login.php" title="Logout">Logout</a></p>
				<img class="logo_login" src="../images/logo-dark.png" alt="logo">
			</div>
			<div class="container-fluid container">
				<div class="row d-block d-sm-flex">
					<div class="col-12">
						<h2>Modifica post</h2>

						<?php
						$existingFiles = array();
						while ($row = $result->fetch_assoc()) {
							$id = $row['id'];
							$title = $row['title'];
							$content = $row['content'];
							$date = $row['date'];
							$files = explode(",", $row['files']);
							$existingFiles = explode(",", $row['files']);
				?>	
						<form action="edit_post.php" enctype="multipart/form-data" method="post" class="w-100">
							<div class="form-group">
								<label>Titolo del post</label><br>
								<input type="text" hidden name="id" value="<?php echo $id ?>">
								<input class="w-100" type="text" name="title_post" placeholder="Titolo del post" required value="<?php echo $title; ?>">
							</div>
							<div class="form-group">
								<textarea name="article_post" placeholder="Testo del post"><?php echo html_entity_decode($content); ?></textarea>
							</div>
							<div class="form-group">
								<label>Immagine del post</label><br>
								<?php foreach ($existingFiles as $file) { ?>
									<img class="w-25 float-left px-2 my-3" src="<?php echo '../upload/' . $file ?>">
								<?php } ?>
								<div class="clearfix"></div>
								<input type="file" name="files[]" multiple><br>
								
							</div>
							<div class="form-group">
								<label>Data di pubblicazione</label><br>
								<input type="date" name="post_date" required value="<?php echo $date; ?>"><br>
							</div>
							<div class="clearfix"></div>
							<input type="submit" name="edit_post" value="Modifica">
							<a class="annulla" href="../admin/index.php">Annulla</a>
						</form>
						<?php } ?>
					</div>
				</div>
			</div>
		</body>
    </html>

<?php include "../includes/config_locale.php" ?>
<?php 
	if (isset($_GET['id'])) {
        $id= $_REQUEST['id'];
        $sql = "SELECT * FROM posts WHERE id=$id";
        $result = $conn->query($sql); 
	}
    while ($row = $result->fetch_assoc()) {
        $id = $row['id'];
        $title = $row['title'];
        $content = $row['content'];
        $date = $row['date'];
        $files = explode(",", $row['files']);     

?>
<?php include "../header.html"; ?>
        <title></title><!--titolo-->
    </head>
    <body class="blog-template">
        <?php include "../menu.html"; ?>
            <div class="first">
                <div class="container-fluid">
                    <div class="logo">
                        <a href="/" title="home"><img src="/fede2_orig/img/logo.svg" alt="logo"></a>
                    </div>        
                </div>
                <div class="container-fluid blog-wrapper">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <h2 class="title"><?php echo $row['title'] ?></h2>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <p class="testo">
                            <?php echo html_entity_decode($row['content']); ?></p>
                            <p class="date">Data di pubblicazione: <?php echo date('d/m/Y', strtotime($row['date'])) ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div>
                <?php include "../footer.html"; ?>
            </div>
        <!-- Script -->
        <script src="/fede2_orig/js/jquery-3.4.1.min.js"></script>
        <script src="/fede2_orig/js/cookiechoices.js"></script>
        <script src="/fede2_orig/js/bootstrap.min.js"></script>
        <script src="/fede2_orig/js/pageable.js"></script>
        <script src="/fede2_orig/js/in-view.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/gsap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/ScrollTrigger.min.js"></script>
        <script src="/fede2_orig/js/script.js"></script>            
    </body>
</html>

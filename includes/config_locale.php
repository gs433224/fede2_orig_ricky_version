<?php
//connection
$conn = mysqli_connect("localhost", "root", "", "test_carlominot");
if(!$conn) {
    die("Errore nella connessione:" . mysqli_connect_error());
}


//crea post
if(isset($_REQUEST['create_post'])) {

    $title = htmlspecialchars($_REQUEST['title_post'], ENT_QUOTES, 'UTF-8');
    $article = htmlspecialchars($_REQUEST['article_post'], ENT_QUOTES, 'UTF-8');
    $date = $_REQUEST['post_date'];

    $files = array();
    foreach ($_FILES['files']['name'] as $i => $name) {
        if ($_FILES['files']['error'][$i] == 0) {
            $temp = $_FILES['files']['tmp_name'][$i];
            $path = "../upload/" . $name;
            move_uploaded_file($temp, $path);
            $files[] = $name;
        }
    }
    $sql = "INSERT INTO posts (title, content, files, date) VALUES ('$title', '$article', '".implode(",", $files)."', '$date')";
    $result = $conn->query($sql);
    if ($result) {
        $Message = urlencode("Post creato con successo");
        header("Location:index.php?message=".$Message);
        $conn->close();
        exit();
    } else {
        echo "Error submitting post: " . $conn->error;
    }
}

//modifica post
if (isset($_REQUEST['edit_post'])) {
    $id = $_REQUEST['id'];
    $title = htmlspecialchars($_REQUEST['title_post'], ENT_QUOTES, 'UTF-8');
    $article = htmlspecialchars($_REQUEST['article_post'], ENT_QUOTES, 'UTF-8');
    $date = $_REQUEST['post_date'];

    $files = array();
    if (isset($_FILES['files']['name'][0]) && !empty($_FILES['files']['name'][0])) {
        // Ci sono nuovi file caricati
        foreach ($_FILES['files']['name'] as $i => $name) {
            if ($_FILES['files']['error'][$i] == 0) {
                $temp = $_FILES['files']['tmp_name'][$i];
                $path = "../upload/" . $name;
                move_uploaded_file($temp, $path);
                $files[] = $name;
            }
        }
    } else {
        // Nessun nuovo file caricato, mantieni le immagini esistenti
        $existingFiles = explode(',', $_POST['existing_files']);
        $files = $existingFiles;
    }

    $sql = "UPDATE posts SET title='$title', content='$article', files='".implode(",", $files)."', date='$date' WHERE id=$id";
    $result = $conn->query($sql);
    if ($result) {
        $Message = urlencode("Post modificato con successo");
        header("Location:index.php?message=".$Message);
        $conn->close();
        exit();
    } else {
        echo "Error submitting post: " . $conn->error;
    }
}


//elimina post
if(isset($_REQUEST['delete_post'])) {
    
    $id = $_REQUEST['id'];
    $title = htmlspecialchars($_REQUEST['title_post'], ENT_QUOTES, 'UTF-8');
    $article = htmlspecialchars($_REQUEST['article_post'], ENT_QUOTES, 'UTF-8');

    $sql = "DELETE FROM posts WHERE id=$id";
    $result = $conn->query($sql);
    if ($result) {
        $Message = urlencode("Post eliminato con successo");
        header("Location:index.php?message=".$Message);
        $conn->close();
        exit();
    } else {
        echo "Error submitting post: " . $conn->error;
    }
}


?>